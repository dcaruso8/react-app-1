import React, { useContext, useState } from 'react';
import Navigation from '../components/Navigation';
import Logo from '../components/Logo';
import { UserContext } from '../UserContext';
import { Form, Input, Button, Checkbox } from 'antd';

const Login = () => {
    const { user, setUser } = useContext(UserContext)
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const login = {
        "username": "dcaruso",
        "password": "ilovego"
    }

    return (
        <div>
            <Logo />
            <Navigation />
            <h1>Login</h1>

            <Form
                name="loginForm"
                labelCol={{ span: 8 }}
                wrapperCol={{ span: 16 }}
                initialValues={{ remember: true }}
                autoComplete="off"
            >
                <Form.Item
                    label="Username"
                    name="username"
                    rules={[{ required: true, message: 'Please input your username!' }]}
                >
                    <Input onChange={(e) => setUsername(e.target.value)}/>
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password onChange={(e) => setPassword(e.target.value)}/>
                </Form.Item>

                <Form.Item name="remember" valuePropName="checked" wrapperCol={{ offset: 8, span: 16 }}>
                    <Checkbox>Remember me</Checkbox>
                </Form.Item>

                <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                    <Button type="primary" htmlType="submit" onClick={() => setUser({ "username": username, "password": password})}>
                        Submit
                    </Button>
                </Form.Item>
            </Form>
            {username === login.username && password === login.password && <p>User succesfully authenticated</p>}
        </div>
    );
};

export default Login;