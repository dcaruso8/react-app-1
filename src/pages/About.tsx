import React, { useContext } from 'react';
import Navigation from '../components/Navigation';
import Logo from '../components/Logo';
import { UserContext } from '../UserContext';

const About = () =>{
    const {user, setUser} = useContext(UserContext)

    return(
        <div>
            <Logo/>
            <Navigation/>
            <pre>{JSON.stringify(user, null, 2)}</pre>
            <h1>A propos</h1>
            <br/>
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Molestias sed deserunt in quibusdam doloremque quo consectetur omnis, quidem eos laboriosam et recusandae necessitatibus accusantium nihil voluptate nisi blanditiis cupiditate ut?</p>
            <br/>
            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Odit repellat sunt quam explicabo nihil beatae maxime praesentium possimus iure repudiandae tenetur iste aut qui ut dolores, perspiciatis placeat, nostrum sapiente quae minima alias. Commodi quam velit beatae voluptates minima soluta voluptatibus, officia tempora molestiae, molestias, nisi reprehenderit asperiores! Repellat non, neque possimus aliquid asperiores iusto cupiditate itaque enim reiciendis! Officia repellendus ab adipisci quasi obcaecati deserunt natus iusto laboriosam dolor, ipsum necessitatibus exercitationem dolorum accusamus veritatis! Sed dolor rem sint reprehenderit officia, aliquid impedit est possimus, soluta, quam fuga fugit nemo necessitatibus dignissimos ex doloremque fugiat illum? Reiciendis, eos, magnam doloremque optio earum perspiciatis explicabo esse dolorem quas libero mollitia ducimus itaque. Quibusdam ad voluptates reiciendis facilis commodi dolorem, culpa exercitationem, quam, distinctio consequatur blanditiis! Et quos quam vitae debitis, culpa provident modi quod itaque eaque possimus architecto eius suscipit. Architecto assumenda asperiores quisquam atque ea. Accusantium, obcaecati! Sunt veritatis eveniet illum voluptate. Optio fugiat beatae cupiditate explicabo ad est atque deserunt, neque culpa sint commodi maiores porro consequuntur nemo, eum rerum qui, modi debitis quaerat? Sapiente nam corporis illum officiis autem odit aspernatur reprehenderit, adipisci quaerat nostrum temporibus facere dolore, aliquam, repellendus laborum quis. Nisi sit a laboriosam consequatur!</p>
        </div>
    );
};

export default About;