import React, { useContext } from 'react';
import Navigation from '../components/Navigation';
import Logo from '../components/Logo';
import Countries from '../components/Countries';
import { UserContext } from '../UserContext';

const Home = () =>{
    const {user, setUser} = useContext(UserContext)
    return(
        <div>
            <Logo/>
            <h1>Welcome back {user != undefined ? user.username : "Unknown"}</h1>
            <Navigation/>
            <Countries/>
        </div>
    );
};

export default Home;