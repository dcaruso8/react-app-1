import React, { useState, useMemo } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import About from "./pages/About";
import Home from "./pages/Home";
import Login from "./pages/Login";
import { UserContext } from "./UserContext";

const App = () =>{
  const [user,setUser] = useState(null)
  const providerValue = useMemo(()=>({user, setUser}),[user, setUser])

  return(
    <BrowserRouter>
      <UserContext.Provider value={{user,setUser}}>
      <Routes>
        <Route path="/" element={<Home/>}/>
        <Route path="/about" element={<About/>}/>
        <Route path="/login" element={<Login/>}/>
        <Route path="*" element={<Home/>}/>
      </Routes>
      </UserContext.Provider>
    </BrowserRouter>
  )
}

export default App
